import { useState, useEffect } from "react";

import LoginForm from "./components/LoginForm";
import MoviesRoom from "./components/MoviesRoom";

import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./services/auth";
import { Route, Routes, Navigate } from "react-router-dom";
import { useNavigate } from 'react-router';

import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

import "./App.css";


function App() {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();
  /**
   * useEffect hook to handle authentication
   * via firebase API 
   */
  useEffect(() => {
    onAuthStateChanged(auth, (usr) => {
      setUser(usr);
      if(usr == null)
        navigate('/login');
        
    });
  }, [user, navigate]);


  return (
      <Routes>
        <Route
          path='/login'
          element={
            !user ? 
            <LoginForm/> : <Navigate to='/'/>
          }/>

        <Route
          path='*'
          element={
            user // this handles the delay at refresh time 
            && 
            <MoviesRoom user={user}/>
          }/>
      </Routes>
  );
}

export default App;
