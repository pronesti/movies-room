import { initializeApp } from "firebase/app";
import firebaseConfig from "./firebase_config"

// Initialize Firebase
export const firebase = initializeApp(firebaseConfig);