// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA2bpO9a0P942NpcKzNLoj-nCvmMrxe8a0",
    authDomain: "movies-room.firebaseapp.com",
    databaseURL: "https://movies-room-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "movies-room",
    storageBucket: "movies-room.appspot.com",
    messagingSenderId: "17554126104",
    appId: "1:17554126104:web:b6ff635a9f392558b31a6d",
    measurementId: "G-KPWSMFR7GV"
};

export default firebaseConfig;