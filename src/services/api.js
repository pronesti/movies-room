import { getDatabase, ref } from "firebase/database";
import { getFirestore, collection, query, where, getDocs, doc, setDoc, deleteDoc} from "firebase/firestore";

import { firebase } from './app';

// RTDatabase
const db = getDatabase(firebase);
const REAL_TIME_DB = "movies-list";

// Firestore
const firestore = getFirestore(firebase);
const FIRESTORE_DB = "movies-wishlist";


/**
 * Adds movie to the wishlist
 * @param {*} row 
 * @param {*} user 
 */
async function addToWishlist(row, user) {
    // set the additional field to mark
    // which user's wishlist the movie belongs to
    row['user'] = user.email;

    const docId = user.email + row.id.toString();
    const newMovieInWishlist = doc(firestore, FIRESTORE_DB, docId);
    
    try{
        await setDoc(newMovieInWishlist, row);
    } catch (error) {
        throw error;
    }
    // returning the id of the new doc
    // in case needed from outside 
    return docId;
}

/**
 * Removes movie from the wishlist
 * @param {*} row 
 * @param {*} user 
 */
async function removeFromWishlist(row, user) {
    const id = user.email + row.id.toString();
    const movieToRemove = doc( firestore, FIRESTORE_DB, id);

    try {
        await deleteDoc(movieToRemove);
    } catch (err) {
        throw err;
    }
}

/**
 * Retrieves the wishlist for
 * the given user
 * @param {*} user 
 * @returns 
 */
async function getWishList(user) {
    const moviesCollection =  collection(firestore, FIRESTORE_DB);

    // construct a query to get the wishlist
    // belonging to the specific user from firestore
    const q = query(
      moviesCollection,
      where("user", "==", user.email)
    );
    
    // query the database using q
    const wishlist = await getDocs(q);
    return wishlist;
}

/**
 * Retrieves an iterator to the movies
 * to be traversed in the client
 * @returns 
 */
function getMoviesRef(){
    // returns a ref from the firebase 
    // real time database
    return ref(db, REAL_TIME_DB);
}


const API = { addToWishlist, removeFromWishlist, getWishList, getMoviesRef };
export default API;