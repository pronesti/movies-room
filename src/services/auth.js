import { getAuth,  GoogleAuthProvider, signInWithPopup } from "firebase/auth"
/* eslint-disable */
import { firebase }  from './app';

export const auth = getAuth();

// Google auth
const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });


/**
 * Handles the signin with google via the `SignInWithPop` 
 * from firebase
 * @returns 
 */
 export const signInWithGoogle = () => signInWithPopup(auth, provider)
                        .catch((error) => {
                            console.log(error.code);
                            console.log(error.message);
                        });