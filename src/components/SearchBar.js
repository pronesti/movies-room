import "../App.css";

import { FormControl, Form } from "react-bootstrap";


function SearchBar(props){

  /**
   * Handles the text changes in the
   * search bar
   * @param {*} e : change event
   */
  const handleForm = (e) => {
    e.preventDefault();
    props.setFilterQuery(e.target.value);
  }

  return (
    <Form className="mb-3 below-nav">
      <FormControl onChange={handleForm} placeholder='Search movies'/>
    </Form>
  );
}

export default SearchBar;
