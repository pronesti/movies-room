import { signInWithGoogle } from "../services/auth";
import { Button, Container, Row } from "react-bootstrap";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";

import logo from "../assets/logo.png";
import "../App.css";


function LoginForm () {
  return (
    <div className="center">
      <div className="login">
        <Container fluid>
          <Row>
              <h1>Movies Room</h1>
          </Row>
          
          <Row>
              <img className="image-logo" src={logo} alt="Logo" />
          </Row>

          <Row className="login-btn">
              <Button onClick={signInWithGoogle} variant="danger">
                <FontAwesomeIcon className="google-logo" icon={faGoogle} />
                Login
              </Button>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default LoginForm;
