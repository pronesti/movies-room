import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import {Button} from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import "../App.css"

/** This component handles the table of movies, i.e. the main page and 
 * the wishlist using react-bootstrap table and paginator
 * https://github.com/react-bootstrap-table/react-bootstrap-table2
 *  
 * doc used:
 * https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/getting-started.html
 * and slides from prof. F. Corno from PoliTO
 */


function MoviesTable(props){
    /**
     * 
     * @param {*} cell 
     * @param {*} row 
     * @returns 
     */
    const actionsFormatter = (cell, row) => (      
        <Button onClick={() => props.action(row)} variant="outline-danger">
            <FontAwesomeIcon icon={props.isWishlist ? faMinusCircle : faHeart}/>
        </Button>
        
    );
    
    // column structure for the
    // boostrap table
    const columns = [
        {
            dataField: "title",
            text: "Title",
            sort: true
        },
        {
            dataField: "year",
            text: "Year",
            sort: true
        },
        {
            dataField: "genre",
            text: "Genre",
            sort: true
        },
        {
            dataField: "actions",
            text: props.isWishlist ? "Remove from wishlist" : "Add to wishlist",
            isDummyField: true,
            csvExport: false,
            formatter: actionsFormatter
        }
    ];

    // adding style settings to each 
    // column
    columns.forEach(col => {
        col.align = "center";
        col.headerAlign = "center";
        col.headerStyle = { border: "none" };
        col.style = { border: "none" }; 
    });
    
    const defaultSorted = [
    {
        dataField: "id",
        order: "asc" // ascending order 
    }
    ];

    // describes the pagination employing
    // the paginationFactory
    const paginationOptions = {
        page: 1,
        sizePerPage: 10,
        lastPageText: ">>",
        firstPageText: "<<",
        nextPageText: ">",
        prePageText: "<",
        showTotal: true,
        alwaysShowAllBtns: true,
        onPageChange: function (page, sizePerPage) {},
        onSizePerPageChange: function (page, sizePerPage) {}
    };

    return (
        <BootstrapTable
            keyField="id"
            data={props.movies}
            columns={columns}
            defaultSorted={defaultSorted}
            pagination={paginationFactory(paginationOptions)}
        />
    );
}

export default MoviesTable;
