import "../App.css";
import { useState } from "react";
import { Spinner } from "react-bootstrap";

import  SearchBar  from "./SearchBar";
import MoviesTable from "./MoviesTable";


function MoviesCatalog (props){
  // states
  const [filterQuery, setFilterQuery] = useState("");

  // unwrapping props
  const { movies, moviesWishlist, addToWishlist, isLoading, filterMovies } = props;

  // getting the movie not in wishlist
  // to display in the catalog of movies
  const moviesNotInWishlist = movies.filter((movie) => {
    return !moviesWishlist.some(mwl => mwl.id === movie.id);
  });

  // get movies filtered according to the search bar input query
  const filteredMovies = filterMovies( 
    moviesNotInWishlist, 
    filterQuery
  );

  return (
    <div className="main-page">
      {isLoading ? 
        <div className="spinner">
          <h1>Loading movies...</h1>
          <Spinner
            style={{ marginLeft: "2em" }}
            animation="border"
            role="status"
          >
            <span className="visually-hidden">Loading... </span>
          </Spinner>
        </div>
       : 
        <>
          <SearchBar setFilterQuery={setFilterQuery} />
          <MoviesTable 
            movies={filteredMovies} 
            action={addToWishlist} 
          />
        </>
      }
    </div>
  );
}

export default MoviesCatalog;
