import "../App.css";
import { useState } from "react";
import { Button, Card } from "react-bootstrap";

import  SearchBar from "./SearchBar";

import { useNavigate } from "react-router-dom";
import MoviesTable from "./MoviesTable";

function Wishlist ({ moviesWishlist, removeFromWishlist, filterMovies }){
  const [filterQuery, setFilterQuery] = useState("");

  const navigate = useNavigate();


  // get movies filtered according to the search bar input query
  const filteredMovies = filterMovies(
    moviesWishlist,
    filterQuery
  );

  return (
    <div className="main-page">
      {moviesWishlist?.length > 0 ? 
        <>
          <SearchBar setFilterQuery={setFilterQuery}/>
          <MoviesTable movies={filteredMovies} action={removeFromWishlist} isWishlist/>
        </>
       : 
        <Card className="below-nav text-center bg-grey" style={{ height: '10rem', border: 'none' }}>
          <Card.Body>
            <Card.Title>Your wishlist is empty</Card.Title>
            <Button
              variant="secondary"
              className="bg-dark"
              onClick={() => {
                // returns to the main page
                navigate("/");
              }}
            >
              Return to the movies list
            </Button>
          </Card.Body>
        </Card>
        
      }
    </div>
  );
}

export default Wishlist;
