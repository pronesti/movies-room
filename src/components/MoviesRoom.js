import React, { useEffect, useState } from "react";
// routing
import { Route, Routes, Navigate } from "react-router-dom";

// firebase and firestore
import { onValue } from "firebase/database";
import API from '../services/api';

// this app's components
import  NavBar  from "./NavBar";
import  MoviesCatalog  from "./Catalog";
import  Wishlist  from "./Wishlist";

import "../App.css";


/**
 * This is the main component of the web application 
 * once the user is logged one.
 * It routes to `MoviesCatalog` (main page) or `WishList`
 * according to the path 
 */
const MoviesRoom = ({ user }) => {
  // states
  const [movies, setMovies] = useState([]);
  const [moviesWishlist, setMoviesWishlist] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  /**
   * useEffect hook to fetch data from firebase
   * realtime database and firestore database and
   * load it into the `movies` state and into the 
   * `whishlist` state
   */
  useEffect(() => {
    // loading all movies
    onValue(API.getMoviesRef(), (snapshot) => {

      let _movies = [];
      snapshot.forEach((snap) => {
        _movies.push(snap.val());
      });

      setMovies(_movies);
      setIsLoading(false);
    });

    // loading wishlist
    API.getWishList(user)
       .then((snapshot) => {
          let _wishlist = [];

          snapshot.forEach((doc) => {
            _wishlist.push(doc.data());
          });
          setMoviesWishlist(_wishlist);
        });
    }, [user]);

  /**
   * Adds a movie to the wishlist
   * updating the firestore database
   * and the state of webapp
   * @param {*} row 
   */
  const addToWishlist = (row) => {    
    // make the change persistent
    API.addToWishlist(row, user)
      .catch((error) => {
        console.log(`Unsuccessful returned error ${error}`);
      });
    
    // update the state to trigger the useEffect hook
    // to update the visualization
    setMoviesWishlist([...moviesWishlist, row]);
  };

  /**
   * Removes a movie from the wishlist
   * updating the firestore database
   * and the state of webapp
   * @param {*} row 
   */
  const removeFromWishlist = (row) => {
    // make the remove persistent in firestore
    API.removeFromWishlist(row, user)
       .catch((error) => {
          console.log(`Unsuccessful returned error ${error}`);
        });
    // make the change effective in the web app
    setMoviesWishlist(moviesWishlist.filter((movie) => movie.id !== row.id));
  };

  /**
   * Filters movies according 
   * to the categories title, year, genre (all at once) 
   * using the keywords in the input bar
   * @param {*} movies 
   * @param {*} searchQuery
   * @returns 
   */
  const filterMovies = (movies, searchQuery) => { 
    if (searchQuery.trim() !== "") {
       return movies.filter((movie) => {
            return ['title', 'genre', 'year']
                         .some( k => movie[k].toString()
                                             .toLowerCase()
                                             .includes(searchQuery.toLowerCase())
                              );
       });
    }
    return movies;
  };

  
  return (
      <div className="home">
        <NavBar user={user} />
        <Routes>
          {/* "/" route for main page (catalog) */}
          <Route
            path="/"
            element={
              <MoviesCatalog
                movies={movies}
                moviesWishlist={moviesWishlist}
                addToWishlist={addToWishlist}
                isLoading={isLoading}
                filterMovies={filterMovies}
              />
            }
          />

          {/* "/wishlist" route for wishlist */}
          <Route
            path="/wishlist"
            element={
              <Wishlist
                moviesWishlist={moviesWishlist}
                removeFromWishlist={removeFromWishlist}
                filterMovies={filterMovies}
              />
            }
          />

          {/* redirect route (`Redirect` deprecated in react-router-dom v6)*/}
          <Route path="*"
            element={
              <Navigate replace to='/'/>
            }
            />
        </Routes>
      </div>
  );
};

export default MoviesRoom;
