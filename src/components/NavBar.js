import "../App.css";
import { useLocation } from 'react-router-dom';
import { Button, Navbar, Container, Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import logo from "../assets/logo.png";

import { NavLink } from "react-router-dom";

import { signOut } from "firebase/auth";
import { auth } from "../services/auth";

function NavBar(props){
  
  const { user } = props;
  const location = useLocation()

  const logout = () => {
    signOut(auth)
      .then(() => {
        console.log("logged out")
      })
      .catch((error) => {
        // An error happened.
      });
  };

  return (
    <Navbar bg="dark" variant="dark" fixed="top">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/" exact>
          <img className="navbar-logo" src={logo} alt="Logo" />
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto"></Nav>
          <Nav>
            <Nav.Link>{user.displayName ? user.displayName : "user"}</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        
        <Button className="wishlist-btn" as={NavLink}
          to={location.pathname === "/wishlist" ? "/" : "/wishlist"}
          variant={location.pathname === "/wishlist" ?  "danger" : "outline-danger"}
        >
          <FontAwesomeIcon icon={faHeart} />
        </Button>
        
        <Button className="wishlist-btn" variant="danger" onClick={() => logout()}>
          Logout
        </Button>
      </Container>
    </Navbar>
  );
}

export default NavBar;