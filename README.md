# Movies Room

This is a simple web application developed using [react](https://it.reactjs.org/), [fortAwsome](https://fontawesome.com/v5.15/icons?d=gallery&p=2) and [firebase](https://firebase.google.com/).

It allows to log in from google and manage a personal wishlist of movies from a static real time database.

Browse movies room [here](https://movies-room.web.app)

## Demo

### Login (with Google) form
[![login](images/login.png)]()

### Movies Catalog
[![catalog](images/catalog.png)]()

### Movies Wishlist
[![whishlist](images/wishlist.png)]()

### Empty Wishlist
[![empty wishlist](images/empty_wishlist.png)]()

### Search Bar
[![search bar](images/filter.png)]()